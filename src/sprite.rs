use crate::math::Point;
use crate::win32_engine::{Texture, TextureLoader, Win32Engine};

pub enum SpriteType {
    ANIMATED,
    STATIC,
}

// Require sprite sheets to be
// fixed in where the animations for
// characters are the same as this enum?
#[derive(Copy, Clone)]
pub enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN,
    NONE, // For example if the sprite only as one row and no columns
}

pub struct Sprite<'a> {
    texture: &'a Texture,
    sprite_type: SpriteType,
    direction: Option<Direction>,
}

impl<'a> Sprite<'a> {
    pub fn new_static(loader: &'a TextureLoader, file_path: &str) -> Self {
        Self {
            texture: loader.get_texture(file_path).unwrap(),
            sprite_type: SpriteType::STATIC,
            direction: None,
        }
    }

    pub fn new_animated(loader: &'a TextureLoader, file_path: &str) -> Self {
        Self {
            texture: loader.get_texture(file_path).unwrap(),
            sprite_type: SpriteType::ANIMATED,
            direction: Some(Direction::DOWN),
        }
    }

    pub fn draw(&self, pos: Point<u32>, engine: &Win32Engine) {
        match self.sprite_type {
            SpriteType::ANIMATED => {
                // Animated means that the bmp loaded is a sprite sheet
                // so that means we have to draw differently, obviously.
                // not gonna use the .draw_bmp() method.
                self.texture.bmp.draw_bmp(pos, engine);
            }
            SpriteType::STATIC => {
                // Static sprite simple draw.
                self.texture.bmp.draw_bmp(pos, engine);
            }
        }
    }

    pub fn animate(&self, tile_size: Point<u32>, width: u32, height: u32, time: f32) {
        // Sprite sheet means we have clips of textures we want to
        // scroll through to simulate animation.

        // Example:
        /*
           Sprite sheet loaded is 128 x 128.
           Our individual tiles are 64 x 64, so:
              width divided by height = amnt of texture clippings

              128 / 64 = 2 rows of clipings
              128 / 64 = 2 columns of clippings

              The indices would look like:

               0 1
             0 x x
             1 x x
        */

        let y_size = height / tile_size.y; // number of rows
        let x_size = width / tile_size.x; // number of columns

        match self.direction.unwrap() {
            Direction::LEFT => {
                for i in 0..y_size {
                    if time as i32 % 2 == 0 {}
                }
            }
            Direction::RIGHT => {}
            Direction::UP => {}
            Direction::DOWN => {}
            _ => {}
        }
    }
}
