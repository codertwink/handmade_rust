mod language_layer;
mod math;
mod time_manager;
mod win32_engine;

mod entity;
mod entity_manager;
mod sprite;

use entity::Entity;
use entity_manager::EntityManager;
use math::{Point, V2f};
use sprite::Sprite;
use win32_engine::{LoadedBitmap, SoundEngine, TextureLoader, Win32Engine};

//IDEA(S):
// Maybe implement sound buffer
// Create a Sprite Object??

fn main() {
    // Create Win32 window n stuff
    let mut win32_engine = Win32Engine::new("Cheese Game");

    // Create entity manager
    let mut entity_manager = EntityManager::new();

    // Create an entity
    let player = Entity::new(V2f::new(25.0, 25.0), entity::EntityType::RECT);

    // Add entity to entity list
    entity_manager.create(player);

    let test_bmp = LoadedBitmap::load_bmp("Assets/grass.bmpx");

    let textures = TextureLoader::new("Assets");

    let sprite = Sprite::new_static(&textures, "Sprite-0001.bmp");

    let sound = SoundEngine::new();

    while win32_engine.is_running() {
        // Window events
        win32_engine.handle_events();

        // Update -- includes input
        entity_manager.update(&mut win32_engine);

        // Draw
        win32_engine.clear_screen(0x0FFda025);

        test_bmp.draw_bmp(Point::new(25, 25), &win32_engine);

        sprite.draw(Point::new(500, 64), &win32_engine);

        entity_manager.draw(&win32_engine);

        win32_engine.render_buffer_to_screen();
    }
}
