// Standard library
use std::ffi::CString;
use std::fs;
use std::mem;
use std::process::exit;

// Windows' stuff:
use winapi::shared::minwindef::{HINSTANCE, LPARAM, LRESULT, UINT, WPARAM};
use winapi::shared::ntdef::{LPCSTR, LPCWSTR};
use winapi::shared::windef::{HBRUSH, HDC, HICON, HMENU, HWND, RECT};
use winapi::shared::winerror::ERROR_SUCCESS;
use winapi::um::wingdi::{
    GetDeviceCaps, StretchDIBits, BITMAPINFO, BITMAPINFOHEADER, BI_RGB, DIB_RGB_COLORS, RGBQUAD,
    SRCCOPY, VREFRESH,
};

use winapi::um::winnt::{
    FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, GENERIC_READ, HEAP_ZERO_MEMORY, MEM_COMMIT,
    MEM_RELEASE, MEM_RESERVE, PAGE_READWRITE,
};
use winapi::um::winuser::*;

use kernel32::*;
use winapi::um::xinput::{
    XInputGetState, XINPUT_GAMEPAD, XINPUT_GAMEPAD_DPAD_DOWN, XINPUT_GAMEPAD_DPAD_LEFT,
    XINPUT_GAMEPAD_DPAD_RIGHT, XINPUT_GAMEPAD_DPAD_UP, XINPUT_STATE, XUSER_MAX_COUNT,
};

// Sound stuff:
use rg3d_sound::{
    buffer::{DataSource, SoundBuffer},
    context::Context,
    source::{generic::GenericSourceBuilder, SoundSource, Status},
};

// My stuff:
use crate::language_layer::{create_wide_char, INVALID_HANDLE_VALUE, OPEN_EXISTING};
use crate::math::{Color, Point, Rect};
use crate::time_manager::TimeManager;

//NOTE:
/*
    Why do this in rust?
        Because it's fun and easy, yet hard to use at the same time.

    Why do you not program like a rust programmer?
        "Rust programmers" as far as i've seen on the internet
        and "rustaceans" have some of the most unreadable code ever. Also,
        I am modeling this a bit, though with my own style, on handmadehero
        and the way I wrote it in C.
*/

// TODO:
/*
- Read & Write functions -- finished
- load & draw bmp -- finished
- Figure out how to handle alpha when it comes to loading and drawing bmps -- finished
*/

// NOTE:
// Everything here is tentative asf

// Add more?
pub trait Win32Drawable {
    fn draw_rectangle(&self, color: Color, rect: Rect<f32>);
}

static mut IS_WINDOW_CLOSED: bool = false;

pub enum WindowMessages {
    WindowClosed,
}

// Storage for Screen data that excludes the windows bar
pub struct ClientData {
    width: i32,
    height: i32,
}

pub fn get_client_data(window: &HWND) -> ClientData {
    let mut result = ClientData {
        width: 0,
        height: 0,
    };

    let mut rect = RECT {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    };

    unsafe {
        GetClientRect(*window, &mut rect);
    }

    result.width = rect.right - rect.left;
    result.height = rect.bottom - rect.top;

    result
}

// BitmapHeader struct from windows
#[repr(packed)]
pub struct BitmapHeader {
    pub file_type: u16,
    pub file_size: u32,
    pub reserved_1: u16,
    pub reserved_2: u16,
    pub bitmap_offset: u32,
    pub size: u32,
    pub width: i32,
    pub height: i32,
    pub planes: u16,
    pub bits_per_pixel: u16,
    pub compression: u32,
    pub size_of_bitmap: u32,
    pub horz_resolution: i32,
    pub vert_resolution: i32,
    pub colors_used: u32,
    pub colors_important: u32,
    pub red_mask: u32,
    pub green_mask: u32,
    pub blue_mask: u32,
}

// A bmp texture
#[derive(Clone, Copy)]
pub struct LoadedBitmap {
    pub pixels: *mut u32,
    pub width: u32,
    pub height: u32,
}

fn _bit_scan_forward(index: &mut u32, value: u32) -> bool {
    let mut found = false;

    for i in 0..32 {
        if value & (1 << i) != 0 {
            *index = i;
            found = true;
            break;
        }
    }

    found
}

// TODO: Create a draw_clip() method?

impl LoadedBitmap {
    // These functions and methods are meant for BMP Textures
    pub fn load_bmp(file_path: &str) -> LoadedBitmap {
        let mut result = {
            LoadedBitmap {
                pixels: std::ptr::null_mut(),
                width: 0,
                height: 0,
            }
        };

        let file_read = os_read_entire_file(file_path);

        let header = file_read.contents;

        // I think is is now correct.
        let raw = file_read.raw_contents as *mut u8;
        let pixels =
            unsafe { raw.add(header.as_ref().unwrap().bitmap_offset as usize) } as *mut u32;

        result.pixels = pixels;
        result.width = header.unwrap().width as u32;
        result.height = header.unwrap().height as u32;

        println!(
            "Texture Width: {} & Height: {}",
            result.width, result.height
        );

        // I think rust is somehow auto sorting the bits sooo
        // i don't believe this is needed.
        /*
                let red_mask = header.unwrap().red_mask;
                let green_mask = header.unwrap().green_mask;
                let blue_mask = header.unwrap().blue_mask;
                let alpha_mask = !(red_mask | green_mask | blue_mask);

                let mut red_shift = 0;
                let mut green_shift = 0;
                let mut blue_shift = 0;
                let mut alpha_shift = 0;

                let red_found = bit_scan_forward(&mut red_shift, red_mask);
                let green_found = bit_scan_forward(&mut green_shift, green_mask);
                let blue_found = bit_scan_forward(&mut blue_shift, blue_mask);
                let alpha_found = bit_scan_forward(&mut alpha_shift, alpha_mask);

                if !red_found && !green_found && !blue_found && !alpha_found {
                    println!("All of the bitscans failed.");
                }

                let mut source_dest = result.pixels;

                for y in 0..header.unwrap().height {
                    for x in 0..header.unwrap().width {
                        unsafe {
                            let c = *source_dest;

                            *source_dest = (((c >> alpha_shift) & 0xFF) << 24)
                                | (((c >> red_shift) & 0xFF) << 16)
                                | (((c >> green_shift) & 0xFF) << 8)
                                | (((c >> blue_shift) & 0xFF) << 0);
                            source_dest = source_dest.offset(1);
                        }
                    }
                }

                result.pixels = source_dest;
        */

        result
    }

    pub fn _default() -> Self {
        Self {
            pixels: std::ptr::null_mut(),
            width: 0,
            height: 0,
        }
    }

    //NOTE: ONLY CALL ON BMP TEXTURES
    //NOTE: I am super horrible at understanding anything to do with
    // rendering images or drawing or whatever :REee:

    // the problem may be arising from the reading of the file
    // not neccessarily when we try to draw.
    // the top was true, now it draws fine.
    pub fn draw_bmp(&self, pos: Point<u32>, engine: &Win32Engine) {
        unsafe {
            let source_pixels = self.pixels;
            let mut source_row = source_pixels.add((self.width * (self.height - 1)) as usize);

            let dest_pixels = engine.buffer.memory as *mut u32;
            let mut dest_row = dest_pixels.add((pos.y * engine.get_width() + pos.x) as usize);

            for _y in 0..self.height {
                let mut dest = dest_row;
                let mut source = source_row;

                for _x in 0..self.width {
                    // Only render if the alpha channel is > 128
                    if (*source >> 24) > 128 {
                        *dest = *source;
                    }

                    //*dest += 1;
                    dest = dest.add(1);
                    //*source += 1;
                    source = source.add(1);
                }

                //*dest_row += engine.get_width();
                //*source_row -= self.width;
                let width = self.width as i32;
                dest_row = dest_row.add(engine.get_width() as usize);
                source_row = source_row.add(-width as usize);
            }
        }
    }

    pub fn draw_bmp_clip(&self, src_rect: Rect<u32>, dst_rect: Rect<u32>, engine: &Win32Engine) {}
}

// Screen buffer
pub struct Win32Buffer {
    pub bitmap_info: BITMAPINFO,
    pub memory: *const winapi::ctypes::c_void,
}

impl Win32Buffer {
    // This creation function is just meant for the background buffer
    pub fn new(window: &HWND) -> Self {
        // Buffer struct initialization
        let buffer: BITMAPINFO = BITMAPINFO {
            bmiHeader: BITMAPINFOHEADER {
                biSize: mem::size_of::<BITMAPINFOHEADER>() as u32,
                biWidth: get_client_data(window).width,
                biHeight: -get_client_data(window).height, // negative so it renders from the top left
                biPlanes: 1,
                biBitCount: 32,
                biCompression: BI_RGB,
                biSizeImage: 0,
                biXPelsPerMeter: 0,
                biYPelsPerMeter: 0,
                biClrUsed: 0,
                biClrImportant: 0,
            },
            bmiColors: [RGBQUAD {
                // Array with one struct
                rgbBlue: 0,
                rgbGreen: 0,
                rgbRed: 0,
                rgbReserved: 0,
            }],
        };

        // Buffer and Memory declaration and assignment
        let buffer_size = get_client_data(window).width
            * get_client_data(window).height
            * mem::size_of::<u32>() as i32;
        let mut _memory = 0 as *const winapi::ctypes::c_void;

        // Fill the buffer
        unsafe {
            // If the buffer is not null, clear it
            if !_memory.is_null() {
                VirtualFree(_memory as *mut std::ffi::c_void, 0, MEM_RELEASE);
            }

            // Allocate the memory for the buffer
            _memory = VirtualAlloc(
                std::ptr::null_mut(),
                buffer_size as u64,
                MEM_COMMIT | MEM_RESERVE,
                PAGE_READWRITE,
            ) as *const winapi::ctypes::c_void;
        }

        Self {
            bitmap_info: buffer,
            memory: _memory,
        }
    }
}

unsafe extern "system" fn window_proc(
    h_wnd: HWND,
    msg: UINT,
    w_param: WPARAM,
    l_param: LPARAM,
) -> LRESULT {
    if msg == WM_CLOSE {
        IS_WINDOW_CLOSED = true;
        PostQuitMessage(0);
    }

    DefWindowProcW(h_wnd, msg, w_param, l_param)
}

// General win32 reading
pub struct ReadResult<'a> {
    pub contents: Option<&'a BitmapHeader>,
    pub raw_contents: *mut BitmapHeader,
    pub size: u64,
}

pub fn os_read_entire_file(file_path: &str) -> ReadResult {
    let mut result = ReadResult {
        contents: None,
        raw_contents: std::ptr::null_mut(),
        size: 0,
    };

    let file_path_converted = CString::new(file_path).unwrap();

    unsafe {
        // Get file
        let file_handle = CreateFileA(
            file_path_converted.as_ptr() as LPCSTR,
            GENERIC_READ,
            FILE_SHARE_READ,
            std::ptr::null_mut(),
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            std::ptr::null_mut(),
        );

        // If the file handle is -1 there's a problem
        /* if file_handle == INVALID_HANDLE_VALUE as *mut std::ffi::c_void {
            println!("File handle wrong. Function: os_read_entire_file()");
            exit(1);
        }*/

        // If the file handle is -1 there's a problem
        assert!(
            file_handle != INVALID_HANDLE_VALUE as *mut std::ffi::c_void,
            "File handle wrong."
        );

        // Read file size
        let file_size = GetFileSize(file_handle as *mut std::ffi::c_void, std::ptr::null_mut());

        result.size = file_size as u64;

        let content_read =
            HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, file_size as u64) as *mut BitmapHeader;
        result.contents = Some(&*content_read); // I don't think I use this, only the raw but :shrug:
        result.raw_contents = content_read;

        //TODO: Fix this readfile check
        //NOTE: 12/1/21 does this still ened to be fixed?
        let bytes_read: *mut u32 = 0 as *mut u32;

        let file_read = ReadFile(
            file_handle as *mut std::ffi::c_void,
            content_read as *mut std::ffi::c_void,
            file_size,
            bytes_read,
            std::ptr::null_mut(),
        );

        // Possibly change if checks for asserts - though not too worried about
        // such a simple thing atm.
        assert!(
            file_read != 0,
            "Failed to read file. Function: os_read_entire_file()"
        );

        /* if file_read != 0 {
            // Success
            println!("Reading file was a success!");
        } else {
            println!("Failed to read file. Function: os_read_entire_file()");
            exit(1);
        }*/

        CloseHandle(file_handle);
    }

    result
}

//NOTE:
/*
    Public members of this struct:
    No sense in simply making get methods for everything

    //TLDR; the public members of this struct are convenient
*/

pub struct Win32Engine {
    running: bool,
    buffer: Win32Buffer,
    hwnd: HWND,
    screen_data: ClientData,
    pub time_manager: TimeManager,
    device_context: HDC,
    pub input: Win32Input, // Win32 xinput (only works for xbox controllers)
}

impl Win32Engine {
    pub fn new(window_name: &str) -> Self {
        unsafe {
            let window_class = WNDCLASSW {
                style: 0,
                lpfnWndProc: Some(window_proc),
                cbClsExtra: 0,
                cbWndExtra: 0,
                hInstance: 0 as HINSTANCE,
                hIcon: 0 as HICON,
                hCursor: 0 as HICON,
                hbrBackground: 16 as HBRUSH,
                lpszMenuName: 0 as LPCWSTR,
                lpszClassName: create_wide_char("MyWindowClass").as_ptr(),
            };

            let error_code = RegisterClassW(&window_class);

            assert!(error_code != 0, "Failed to register the window class.");

            let window = CreateWindowExW(
                0,
                create_wide_char("MyWindowClass").as_ptr(),
                create_wide_char(window_name).as_ptr(),
                WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                0 as HWND,
                0 as HMENU,
                0 as HINSTANCE,
                std::ptr::null_mut(),
            );

            assert!(window != (0 as HWND), "Failed to open the window.");

            ShowWindow(window, SW_SHOW);
            UpdateWindow(window);

            let device_context = GetDC(window);

            // Get montitor refresh rate
            let refresh_rate = GetDeviceCaps(device_context, VREFRESH);

            // way to hide console
            /*
            let console_window = GetConsoleWindow();
            ShowWindow(console_window as *mut HWND__, SW_HIDE);
            */

            Self {
                running: true,
                buffer: Win32Buffer::new(&window),
                hwnd: window,
                screen_data: get_client_data(&window),
                time_manager: TimeManager::new(Some(refresh_rate as f32)),
                device_context: device_context,
                input: Win32Input::new(),
            }
        }
    }

    pub fn process_window_messages(&self) -> Option<WindowMessages> {
        unsafe {
            let mut msg: MSG = std::mem::zeroed();

            // Process messages
            while PeekMessageA(&mut msg, self.hwnd, 0, 0, PM_REMOVE) > 0 {
                TranslateMessage(&msg);
                DispatchMessageA(&msg);

                if IS_WINDOW_CLOSED {
                    return Some(WindowMessages::WindowClosed);
                }
            }

            None
        }
    }

    fn resize_buffer(&mut self, width: i32, height: i32) {
        let buffer_size = width * height * mem::size_of::<u32>() as i32;

        unsafe {
            // Only resize if the window is not minimized
            if !self.is_minimized() {
                if !self.buffer.memory.is_null() {
                    VirtualFree(self.buffer.memory as *mut std::ffi::c_void, 0, MEM_RELEASE);
                }

                // Refill the buffer info
                self.buffer.bitmap_info = BITMAPINFO {
                    bmiHeader: BITMAPINFOHEADER {
                        biSize: mem::size_of::<BITMAPINFOHEADER>() as u32,
                        biWidth: width,
                        biHeight: -height,
                        biPlanes: 1,
                        biBitCount: 32,
                        biCompression: BI_RGB,
                        biSizeImage: 0,
                        biXPelsPerMeter: 0,
                        biYPelsPerMeter: 0,
                        biClrUsed: 0,
                        biClrImportant: 0,
                    },
                    bmiColors: [RGBQUAD {
                        rgbBlue: 0,
                        rgbGreen: 0,
                        rgbRed: 0,
                        rgbReserved: 0,
                    }],
                };

                // Re-allocate the buffer - allocate the new buffer size
                self.buffer.memory = VirtualAlloc(
                    std::ptr::null_mut(),
                    buffer_size as u64,
                    MEM_COMMIT | MEM_RESERVE,
                    PAGE_READWRITE,
                ) as *const winapi::ctypes::c_void;
            }
        }
    }

    pub fn clear_screen(&self, color: u32) {
        unsafe {
            /*  let pixel = buffer.memory as *mut u32;

            let mut counter = 0;
            for _y in 0..self.screen_data.height {
                for _x in 0..self.screen_data.width {
                    *pixel.add(counter) = color;
                    counter += 1;
                    // Confused me because in C
                    // it's *pixel++ = color;
                }
            }*/

            if !self.is_minimized() {
                // Faster:
                libc::memset(
                    self.buffer.memory as *mut libc::c_void,
                    color as i32,
                    (self.screen_data.width * self.screen_data.height * 4) as usize,
                );
            }
        }
    }

    pub fn render_buffer_to_screen(&mut self) {
        let current_data = get_client_data(&self.hwnd);

        if self.screen_data.width != current_data.width
            || self.screen_data.height != current_data.height
        {
            // Resize the buffer
            self.resize_buffer(current_data.width, current_data.height);

            // Set new render res
            self.screen_data.width = current_data.width;
            self.screen_data.height = current_data.height;
        }

        // Only render if the window is not minimized.
        if !self.is_minimized() {
            unsafe {
                StretchDIBits(
                    self.device_context,
                    0,
                    0,
                    self.screen_data.width,
                    self.screen_data.height,
                    0,
                    0,
                    self.screen_data.width,
                    self.screen_data.height,
                    self.buffer.memory,
                    &self.buffer.bitmap_info,
                    DIB_RGB_COLORS,
                    SRCCOPY,
                );
            }
        }

        // Every 5 seconds try to get the controller
        if self.time_manager.get_time_elapsed() as u32 % 5 == 0 {
            self.input.get_controller();
        }

        self.time_manager.tick();
    }

    pub fn handle_events(&mut self) {
        while let Some(x) = self.process_window_messages() {
            match x {
                WindowMessages::WindowClosed => {
                    self.running = false;
                    break; // Don't run the rest of the loop
                }
            }
        }
    }

    pub fn is_running(&self) -> bool {
        self.running
    }

    pub fn get_width(&self) -> u32 {
        self.screen_data.width as u32
    }

    pub fn get_height(&self) -> u32 {
        self.screen_data.height as u32
    }

    // Check window focus
    pub fn check_focus(&self) -> bool {
        if unsafe { GetFocus() } == self.hwnd {
            return true;
        }

        false
    }

    // Check if window is minimized
    fn is_minimized(&self) -> bool {
        // If == 0 that means not minimized
        if unsafe { IsIconic(self.hwnd) } == 0 {
            return false;
        }

        // if the value of IsIconic is not zero
        // then the window is minimized.

        true
    }
}

// Win32 Draw functions/methods
impl Win32Drawable for Win32Engine {
    // Thank you Ryan Fleury, I was too stupid to figure this out so I just
    // translated your C code to rust. :) :dumb:
    fn draw_rectangle(&self, color: Color, rect: Rect<f32>) {
        unsafe {
            let pixel = self.buffer.memory as *mut u8; // Starting pixel

            let lower_bound_x = rect.x as u32;
            let lower_bound_y = rect.y as u32;
            let upper_bound_x = lower_bound_x + rect.w as u32;
            let upper_bound_y = lower_bound_y + rect.h as u32;

            let mut _pixel_index = 0;
            for y in lower_bound_y..upper_bound_y {
                for x in lower_bound_x..upper_bound_x {
                    _pixel_index = y * self.screen_data.width as u32 + x;
                    // Pixel index is the pixel coord we wanna change the color of
                    *pixel.add((_pixel_index * 4 + 2) as usize) = color.r * 255;
                    *pixel.add((_pixel_index * 4 + 1) as usize) = color.g * 255;
                    *pixel.add((_pixel_index * 4 + 0) as usize) = color.b * 255;
                }
            }
        }
    }
}

pub struct SoundEngine {
    x: u32,
}

impl SoundEngine {
    pub fn new() -> Self {
        let context = Context::new().unwrap();

        let sound_buffer =
            SoundBuffer::new_generic(DataSource::from_file("Assets/sound.wav").unwrap()).unwrap();

        let source = GenericSourceBuilder::new(sound_buffer)
            .with_status(Status::Playing)
            .build_source()
            .unwrap();

        context.lock().unwrap().add_source(source);

        //std::thread::sleep(std::time::Duration::from_secs(3));

        Self { x: 5 }
    }
}

// NOTE: I don't know if this is a good long term
// implementation. Probably not..
pub struct Win32Input {
    game_pad_state: XINPUT_STATE,
    game_pad_id: i8,
}

impl Win32Input {
    pub fn new() -> Self {
        // Null init XINPUT structures
        let gamepad_struct = XINPUT_GAMEPAD {
            wButtons: 0,
            bLeftTrigger: 0,
            bRightTrigger: 0,
            sThumbLX: 0,
            sThumbLY: 0,
            sThumbRX: 0,
            sThumbRY: 0,
        };

        let state = XINPUT_STATE {
            dwPacketNumber: 0,
            Gamepad: gamepad_struct,
        };

        Self {
            game_pad_state: state,
            game_pad_id: -1,
        }
    }

    pub fn get_controller(&mut self) {
        if self.game_pad_id == -1 {
            let empty_gamepad_struct = XINPUT_GAMEPAD {
                wButtons: 0,
                bLeftTrigger: 0,
                bRightTrigger: 0,
                sThumbLX: 0,
                sThumbLY: 0,
                sThumbRX: 0,
                sThumbRY: 0,
            };

            for i in 0..XUSER_MAX_COUNT {
                let mut state = XINPUT_STATE {
                    dwPacketNumber: 0,
                    Gamepad: empty_gamepad_struct,
                };

                unsafe {
                    if XInputGetState(i, &mut state) == ERROR_SUCCESS {
                        self.game_pad_id = i as i8;
                        println!("Found controller!");
                    }
                }
            }
        }
    }

    pub fn left(&mut self) -> bool {
        unsafe {
            if GetAsyncKeyState(0x41) != 0 || GetAsyncKeyState(VK_LEFT) != 0 {
                return true;
            }

            if XInputGetState(self.game_pad_id as u32, &mut self.game_pad_state) == ERROR_SUCCESS {
                if self.game_pad_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT != 0 {
                    return true;
                }
            }
        }

        false
    }

    pub fn right(&mut self) -> bool {
        unsafe {
            if GetAsyncKeyState(0x44) != 0 || GetAsyncKeyState(VK_RIGHT) != 0 {
                return true;
            }

            if self.game_pad_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT != 0 {
                return true;
            }
        }
        false
    }

    pub fn up(&mut self) -> bool {
        unsafe {
            if GetAsyncKeyState(0x57) != 0 || GetAsyncKeyState(VK_UP) != 0 {
                return true;
            }

            if self.game_pad_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP != 0 {
                return true;
            }
        }
        false
    }

    pub fn down(&mut self) -> bool {
        unsafe {
            if GetAsyncKeyState(0x53) != 0 || GetAsyncKeyState(VK_DOWN) != 0 {
                return true;
            }
            if self.game_pad_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN != 0 {
                return true;
            }
        }
        false
    }
}

fn read_dir(source_folder: &str) -> Vec<String> {
    // Declare the vector that is to be returned
    let mut result = Vec::new();

    // Read files in the directory
    let entries = fs::read_dir(source_folder).unwrap();

    // For every entry load acceptable file
    for entry in entries {
        let entry_name: String = entry
            .as_ref()
            .unwrap()
            .path()
            .into_os_string()
            .to_str()
            .unwrap()
            .to_string(); // only in rust lmao

        // Push every entry name to the result vector
        result.push(entry_name);
    }

    result
}

#[derive(Clone)]
pub struct Texture {
    pub file_name: String,
    pub bmp: LoadedBitmap,
}

impl Texture {
    pub fn new(file_name: String) -> Self {
        let name_clone = file_name.clone();

        Self {
            file_name: name_clone,
            bmp: LoadedBitmap::load_bmp(file_name.as_str()),
        }
    }
}

// Maybe just change to AssetLoader
pub struct TextureLoader {
    textures: Vec<Texture>,
}

impl TextureLoader {
    // Naive implementation but should be ok for now
    pub fn new(source_folder: &str) -> Self {
        // The return vector of textures
        let mut return_textures = Vec::new();

        // Read entire directory of the source folder into Vector
        let files = read_dir(source_folder);

        // Check if each file in the vector is acceptable
        for file in files {
            // Only bmps are acceptable
            if file.contains(".bmp") || file.contains(".bmpx") {
                println!("Name: {}", file);

                // If the file is acceptable push new Texture to vec
                return_textures.push(Texture::new(file));
            }
        }

        Self {
            textures: return_textures,
        }
    }

    // Return reference to texture based on name
    pub fn get_texture(&self, file_name: &str) -> Option<&Texture> {
        let mut return_value = None;

        // If a texture in textures has matching names
        // return that texture, otherwise the return is None.
        for texture in &self.textures {
            if texture.file_name.contains(file_name) {
                return_value = Some(texture);
                break;
            }
        }

        return_value
    }
}
