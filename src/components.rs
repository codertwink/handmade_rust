#[derive(Default, Copy, Clone)]
pub struct ComponentPosition {
    pub x: f32,
    pub y: f32,
}

impl ComponentPosition {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    pub fn move_x(&mut self, amnt: f32) {
        self.x += amnt;
    }
}

#[derive(Default, Copy, Clone)]
pub struct ComponentDraw {
    pub color: u32,
    pub size: u32,
}

impl ComponentDraw {
    pub fn new(color: u32, size: u32) -> Self {
        Self { color, size }
    }
}

#[derive(Default, Copy, Clone)]
pub struct Components<'a> {
    pub position: Option<&'a ComponentPosition>,
    pub draw: Option<&'a ComponentDraw>,
}
